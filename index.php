<?php
session_start();

$con = mysqli_connect("localhost",	"root", 	"", 		"arzttermin");
mysqli_query($con, "SET names utf8"); # Verbindung auf utf-8 umstellen

if(isset($_GET["seite"]) && $_GET["seite"] == "logout")
{
	session_destroy();
	unset($_SESSION);
	setcookie("login_merken", "", time() -1); # cookie entfernen beim Client
	unset($_COOKIE["login_merken"]); # cookie aus dem Server RAM löschen
}

if(isset($_POST["benutzer"]) && isset($_POST["kennwort"]))
{
	$antwort = mysqli_query($con, "select * from arzt_table");
	#print_r( $antwort);

	while($datensatz = mysqli_fetch_array($antwort))
	{
		
		if($_POST["benutzer"] == $datensatz["arzt_email"] && $_POST["kennwort"] == $datensatz["passwort"])
		{
			#echo "klappt";
			$_SESSION["eingeloggt"] = true;
			$_SESSION["benutzer"] = $datensatz["arzt_name"];
			$_SESSION["benutzerEmail"] = $datensatz["arzt_email"];
			$_SESSION["mitteilung"] = "<div style='color:lightgreen'>Erfolgreich eingeloggt</div>";
			if(isset($_POST["merken"]))
			{
				setcookie("login_merken", $datensatz["arzt_name"], time() + 60*60*24*365);
			}
			# Kopfzeilen ändern
			header("Location: ?seite=terminVerwaltung"); # Weiterleiten zur Verwaltung
			exit; # PHP - Programm Ende
		}
	}
	if(!isset($_SESSION["eingeloggt"]))
	{
		#echo "falsch";
		$_SESSION["mitteilung"] = "<div style='color:red'>Falsche Eingabe</div>";
	}	
}

#$con = mysqli_connect("localhost",	"root", 	"", 		"arzttermin");
#mysqli_query($con, "SET names utf8"); # Verbindung auf utf-8 umstellen
#$sql="SELECT * FROM termintable";
#$res=mysqli_query($con,$sql);
#print_r($res);
#die
# wenn der cookie da ist
?>

<html>
<head>
	<title>Arzt Termin</title>
	<meta charset="utf-8" />	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap">
	<link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans:ital,wght@1,300&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" href="css/style.css" />	
</head>
<body  >

<header background="flower.jpg">
	<a href="?seite=start">Startseite</a>
	<a href="?seite=terminBuchen">Buchen</a>

	<?php
		if(isset($_SESSION["eingeloggt"]))
		{
			echo '<a href="?seite=terminVerwaltung">Verwaltung</a>';
			echo '<a href="?seite=logout">Logout</a>';
			echo " &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;Hallo ".$_SESSION["benutzer"];		
		}
		else
		{
			echo '<a href="?seite=login">Login</a>';
		}	
	?>
</header>

<main >
<?php
if(isset($_SESSION["mitteilung"]))
{
	echo $_SESSION["mitteilung"]; # Anzeigen
	unset($_SESSION["mitteilung"]); # Entfernen / Löschen
}

# wenn die Seite nicht(!) gesetzt ist
if(!isset($_GET["seite"]))
{
	$_GET["seite"] = "start"; # Startseite einstellen
}

#print_r($_GET);
switch($_GET["seite"])
{
	case "start":
		include("php/startseite.php"); # externe Datei einbinden
	break;
	case "terminBuchen":
		include("php/neuesEntry.php");	 # externe Datei einbinden
	break;
	case "login":
		include("php/login.php"); # externe Datei einbinden
	break;
	case "logout":
		include("php/logout.php"); # externe Datei einbinden	
	break;	
	case "terminVerwaltung":
		if(isset($_SESSION["eingeloggt"]))
		{
			include("php/verwaltung.php"); # externe Datei einbinden
		}
		else
		{
			header("Location: ?seite=login"); # Weiterleitung zum Login
			exit; # Programm verlassen / beenden
		}	
	break;
	case "edit":
		include("php/edit.php"); # externe Datei einbinden	
	break;
	default:
		include("html/404.html"); # externe Datei einbinden
}

?>

</main>

<footer>
Copyright 2021
</footer>

</body>
</html>
<?php
# Datenbankverbindung trennen
#########################################################################
mysqli_close($con);
#########################################################################
?>