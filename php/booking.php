

<form  method='post' onsubmit="return checkdata()" class ="produktvorschau">
<table >
<tr>
<td class="alter3">Datum <input type="date" name="date" id="documentDate"  value="<?= @$currDate; ?>" onchange="javascript:chooseTime()"/></td>
<td class="alter3">Arzt<select name="arztId" id ="arztId" onchange="javascript:chooseTime()" value="<?= @$currArzt; ?>" /><input type="hidden" id="hiddenarzt" value="<?= @$currArzt; ?>" /><br /></td>
<td class="alter3">Time <select name="time" id="time" value="<?= @$currTime; ?>" /><input type="text" id="hiddentime" disabled value="<?= @$currTime; ?>" />
	<!--?php echo date('Y-m-d'); ?-->
	<!-- <option value="08:00:00">8:00</option>
  <option value="09:00:00">9:00</option>
  <option value="10:00:00">10:00</option>
  <option value="11:00:00">11:00</option>
  <option value="13:00:00">13:00</option>
  <option value="14:00:00">14:00</option>
  <option value="15:00:00">15:00</option>
  <option value="16:00:00">16:00</option> -->
  </td><br />
</tr>
<tr >
<td class="alter3" colspan="3" align="right">Description <input type="textarea" style="width: 650px;" name="desc" value="<?= @$currPDesc; ?>" /><br /></td><br />
</tr>
<tr>
<td class="alter3" colspan="3" align="right">Addresse <input type="text" name="address" style="width: 650px;" value="<?= @$currPAddress; ?>" /><br /></td><br />
</tr>
<tr>
<td class="alter3" colspan="3" align="right">Name <input type="text" name="pName" style="width: 650px;" id="pName" value="<?= @$currPName; ?>" /><br /></td><br />
</tr>
<tr>
<td class="alter3" colspan="3" align="right">Phone Nummer<input type="text" style="width: 650px;" name="pPhone" id="pPhone" value="<?= @$currPPhone; ?>" /><br /></td><br />
</tr>
<tr>
<td class="alter3" colspan="3" align="right">Email <input type="email" style="width: 650px;" name="pEmail" id="pEmail" value="<?= @$currPEmail;  ?>" /><br /></td><br />
</tr>
<tr>
<td class="alter3" colspan="3" align="right"><button type="submit"  id="btnSubmit" name="neues_entry_speichern" >Submit</button><button type="reset" >Reset</button></td>
</tr>
</table>
</form>
<script  type="text/javascript">

let checkdata=()=>{
	//alert("hello");				
	//console.log(typeof parseInt(document.getElementById("pPhone").value));
	//console.log(document.getElementById("documentDate").value);
	if(!validDate()){
		alert("Previous dates cannot be booked.");
		return false;
	}else if(!document.getElementById("pName")  || document.getElementById("pName").value ==""){	
		alert("Enter name of patient.");
		return false;
	}else if(!document.getElementById("pPhone")  || document.getElementById("pPhone").value ==""){	
		alert("Enter phone number.");
		return false;
	}else if(document.getElementById("pPhone").value.length<10){
		alert("Phone number must contain 10 digits");
		return false;
	}else {
		var pattern = /^\d+$/;
        if (!pattern.test(document.getElementById("pPhone").value)){
			alert("Phone number can contain only digits.");
			return false;
		}
	}
	if(document.getElementById("pEmail").value !=""){
		var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (!pattern.test(document.getElementById("pEmail").value)){
			alert("Enter valid email.");
			return false;
		}
	}
	return true;
}

let generateOptions=()=>{
	let timeBox= document.getElementById("time");
	var i, L = timeBox.options.length - 1;
	for(i = L; i >= 0; i--) {
      timeBox.remove(i);
	}
	let ele=document.createElement("option");
	ele.text="8:00";
	ele.value="08:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="9:00";
	ele.value="09:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="10:00";
	ele.value="10:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="11:00";
	ele.value="11:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="13:00";
	ele.value="13:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="14:00";
	ele.value="14:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="15:00";
	ele.value="15:00:00";
	timeBox.add(ele);
	ele=document.createElement("option");
	ele.text="16:00";
	ele.value="16:00:00";
	timeBox.add(ele);
}

let fillTime=(e)=>{
//alert("hello fillTime");
	generateOptions();
	
	if((e.target.readyState ==4) && (e.target.status == 200)){
		//var listTime = e.target.responseText;
		//console.log(e.target.responseText);
		var listTime = JSON.parse(e.target.responseText);
		console.log("filltime");
		
		let timeArr = listTime.result;
		let timeBox= document.getElementById("time");
		console.log(timeArr);
		//console.log(timeBox.options.length);
		for(let i=0;i<timeArr.length;i++){
			//console.log(timeArr[i].time_allotted);
			//console.log(typeof timeArr[i].time_allotted);
			for(let j=0;j<timeBox.options.length;j++){
				if(timeBox.options[j].value==timeArr[i].time_allotted)
				{
					timeBox.remove(j);
					break;
				}
			}
		}
		if(timeBox.options.length==0)
			alert("no slots for this date");
			return false;
	}
}

let validDate=()=>{
	let today = new Date()
	//console.log(today)
	//console.log(new Date(document.getElementById("documentDate").value))
	let selectedDate=new Date(document.getElementById("documentDate").value)
	
	/*console.log(selectedDate.getFullYear())
	console.log(selectedDate.getMonth())
	console.log(selectedDate.getDate())
	console.log(today.getFullYear())
	console.log(today.getMonth())
	console.log(today.getDate())*/

	if(selectedDate.getFullYear() > today.getFullYear()){
		return true;
	}else if( (selectedDate.getFullYear() == today.getFullYear()) &&(selectedDate.getMonth() > today.getMonth() )) {
		return true;
	}else if( (selectedDate.getFullYear() == today.getFullYear()) &&(selectedDate.getMonth() == today.getMonth() ) && (selectedDate.getDate() >= today.getDate() ) ){
				return true;
	}
		
	return false
}


let changeDoctor=(e)=>{
	if((e.target.readyState ==4) && (e.target.status == 200)){
		var listHoliday = JSON.parse(e.target.responseText);
		console.log(document.getElementById("btnSubmit"));
		
		let holidayArr = listHoliday.result;
		
		if(holidayArr.length !=0){
			alert("This doctor is not available on this date. Choose another date or another doctor. "+"Holiday from  "+holidayArr[0].start_datum+"  to  "+ holidayArr[0].end_datum)
			document.getElementById("arztId").options[0].selected='selected';
			document.getElementById("arztId").value=0;
			document.getElementById("btnSubmit").disabled=true;
			//alert("Select Doctor");
		}else{
			var req= new XMLHttpRequest();
			req.open("get","php/ajax_time_slot.php?date="+document.getElementById("documentDate").value+"&arzt="+document.getElementById("arztId").value,true);
			req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			req.onreadystatechange= fillTime;
			req.send();
			document.getElementById("btnSubmit").disabled=false;

		}
	}

}

let chooseTime=()=>{
	//alert("hello");
	//console.log("choosetime");
	//console.log(document.getElementById("documentDate").value);
	//console.log("arzt"+document.getElementById("arztId").value);
	if ( validDate()){
		if(document.getElementById("arztId").value==0){
			generateOptions();
		}else{
			
			var req= new XMLHttpRequest();
			req.open("get","php/ajax_holiday_list.php?date="+document.getElementById("documentDate").value+"&arzt="+document.getElementById("arztId").value,true);
			req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			req.onreadystatechange= changeDoctor;
			req.send();			
		}
	}
	else
		alert("choose date after current date")
}

let fillArzt=(e)=>{
	//console.log("fillarzt");
	if((e.target.readyState ==4) && (e.target.status == 200)){
		var listArzt = JSON.parse(e.target.responseText);
		//console.log(listArzt);
		let ele;
		let arztArr = listArzt.result;
		let arztBox= document.getElementById("arztId");
		
		for(let i=0;i<arztArr.length;i++){
			ele=document.createElement("option");
			ele.text=arztArr[i].arzt_name;
			ele.value=arztArr[i].arzt_id;
			if(document.getElementById("hiddenarzt").value ==ele.value){
				ele.selected='selected';
				arztBox.value=document.getElementById("hiddenarzt").value;
			}
			arztBox.add(ele);
		}
		//console.log(arztBox.options[0].value);
		if(document.getElementById("hiddenarzt").value ==""){
				arztBox.options[0].selected='selected';
				arztBox.value=0;
		}
		
		//console.log("h"+document.getElementById("hiddenarzt").value);
		chooseTime();
		//console.log(document.getElementById("hiddenarzt").value);
		//arztBox.selected=document.getElementById("hiddenarzt").value;
	}
}
let generateArztFill=()=>{
	console.log("generateArztFill");
	req= new XMLHttpRequest();
	req.open("get","php/ajax_arzt.php",true);
	req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	req.onreadystatechange= fillArzt;
	req.send();
}

generateArztFill();
</script>

